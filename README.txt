See http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.platform.doc.user%2FgettingStarted%2Fqs-93_project_builder.htm for instructions on setting up the Ant builder in Eclipse.

To manually run the Ant build, right click on 'build.xml' and select Run As -> Ant Build.
This will create a jar file named 'EightPuzzleSolver.jar' in the project root directory.

To run the built jar, execute:

java -jar <jar_file> <puzzle_file> <search_alg>

on the command line.
