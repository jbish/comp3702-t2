import java.util.*;

/**
 * Created by Jordan on 9/08/2016.
 */
public class DepthFirstSearch extends SearchAlg {

    public DepthFirstSearch(EightPuzzleState start, EightPuzzleState goal) {
        super(start, goal);
    }

    public void search() {
    	/* This is -1 to start with so that visiting start node increments to 
    	 * 0 steps */
        int numSteps = -1;
        System.out.println("DFS evaluation order:\n");
        // Use Stack class for our stack
        Stack<EightPuzzleState> stack = new Stack<EightPuzzleState>();
        // Use a hash set to keep of track of what has been visited
        Set<EightPuzzleState> visited = new HashSet<EightPuzzleState>();
        stack.push(getStart());
        EightPuzzleState goal = getGoal();
        while (true) {
            EightPuzzleState current = stack.pop();
            System.out.println(current.toString());
            numSteps++;
            visited.add(current);
            if (current.equals(goal)) {
                break;
            }
            List<EightPuzzleState> successors = current.getSuccessors();
            for (EightPuzzleState successor : successors) {
                if (!visited.contains(successor)) {
                    stack.push(successor);
                }
            }
        }
        System.out.println("Number of steps = " + numSteps + "\n");
    }
}
