import java.util.*;

/**
 * Created by Jordan on 9/08/2016.
 */
public class BreadthFirstSearch extends SearchAlg {

    private Map<EightPuzzleState, EightPuzzleState> visited;

    public BreadthFirstSearch(EightPuzzleState start, EightPuzzleState goal) {
        super(start, goal);
        // Use a hash map for storing the path visited
        visited = new HashMap<EightPuzzleState, EightPuzzleState>();
    }

    public void search() {
    	/* This is -1 to start with so that visiting start node increments to 
    	 * 0 steps */
        int numSteps = -1;
        System.out.println("BFS evaluation order:\n");
        // Use a linked list for the queue
        LinkedList<EightPuzzleState> queue = new LinkedList<EightPuzzleState>();
        queue.add(getStart());
        EightPuzzleState goal = getGoal();
        visited.put(getStart(), null);
        while (true) {
            EightPuzzleState current = queue.removeFirst();
            numSteps++;
            System.out.println(current.toString());
            if (current.equals(goal)) {
                break;
            }
            List<EightPuzzleState> successors = current.getSuccessors();
            for (EightPuzzleState successor : successors) {
                if (!visited.containsKey(successor)) {
                    queue.add(successor);
                    visited.put(successor, current);
                }
            }
        }
        System.out.println("Number of steps = " + numSteps + "\n");
    }

    public void printShortestPath() {
        List<EightPuzzleState> path = new ArrayList<EightPuzzleState>();
        EightPuzzleState current = getGoal();
        /* Traverse backwards through the map until the previous node for the 
        start node (which is null) is reached */
        while (current != null) {
            path.add(current);
            current = visited.get(current);
        }
        // Reverse the path generated so it is from start to goal
        Collections.reverse(path);
        System.out.println("------- BFS Shortest path -------\n");
        // Print out the path
        for (EightPuzzleState state : path) {
            System.out.println(state.toString());
        }
        System.out.println("Shortest path length = " + path.size());
    }
}
