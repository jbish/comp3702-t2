import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Jordan on 8/08/2016.
 */
/**
 * Class to represent the state of the 8-puzzle game.
 * @author Jordan
 *
 */
public class EightPuzzleState {

	// Use a 1D int array to represent the states
    private int[] flatGrid;
    // Location of the zero (movable piece)
    private int zeroRow;
    private int zeroCol;
    
    /**
     * Construct a new 8-puzzle state from a string of space separated numbers.
     */
    public EightPuzzleState(String nums) {
        flatGrid = new int[9];
        // Assume nums is in left to right, bottom to top order
        // Blank space represented by 0
        String[] parts = nums.split(" ");
        for (int i = 0; i < 9; i++) {
            int num = Integer.parseInt(parts[i]);
            flatGrid[i] = num;
            if (num == 0) {
                zeroRow = i/3;
                zeroCol = i%3;
            }
        }
    }

    /**
     * Construct a new 8-puzzle state from a grid and coordinates of the zero.
     */
    private EightPuzzleState(int[] flatGrid, int zeroRow, int zeroCol) {
        this.flatGrid = flatGrid;
        this.zeroRow = zeroRow;
        this.zeroCol = zeroCol;
    }

    /**
     * Get the successor states of this state.
     */
    public List<EightPuzzleState> getSuccessors() {
        List<EightPuzzleState> successors = new ArrayList<EightPuzzleState>();
        // Determine where the zero can move
        int leftRow = zeroRow;
        int leftCol = zeroCol - 1;

        int rightRow = zeroRow;
        int rightCol = zeroCol + 1;

        int upRow = zeroRow - 1;
        int upCol = zeroCol;

        int downRow = zeroRow + 1;
        int downCol = zeroCol;

        // Check if these co-ords are valid and do swap if possible
        if (leftCol > -1 ) {
            successors.add(swapPositions(zeroRow, zeroCol, leftRow, leftCol));
        }

        if (rightCol < 3) {
            successors.add(swapPositions(zeroRow, zeroCol, rightRow, rightCol));
        }

        if (upRow > -1) {
            successors.add(swapPositions(zeroRow, zeroCol, upRow, upCol));
        }

        if (downRow < 3) {
            successors.add(swapPositions(zeroRow, zeroCol, downRow, downCol));
        }

        return successors;
    }

    /**
     * Swap the position (r1, c1) in the grid with (r2, c2) and return a new
     * EightPuzzleState representing this state.
     */
    public EightPuzzleState swapPositions(int r1, int c1, int r2, int c2) {
        int[] newFlatGrid = new int[9];
        /* Figure out indexes in the flat grid that the swap positions
         * correspond to.
         */
        int firstSwapIndex = 3*r1 + c1%3;
        int secondSwapIndex = 3*r2 + c2%3;
        
        // Construct new state
        int newZeroRow = zeroRow;
        int newZeroCol = zeroCol;
        for (int i = 0; i < 9; i++) {
            int val;
            if (i == firstSwapIndex) {
                val = flatGrid[secondSwapIndex];
            } else if (i == secondSwapIndex) {
                val = flatGrid[firstSwapIndex];
            } else {
                val = flatGrid[i];
            }
            newFlatGrid[i] = val;
            if (val == 0) {
            	// update zero position
                newZeroRow = i/3;
                newZeroCol = i%3;
            }
        }
        return new EightPuzzleState(newFlatGrid, newZeroRow, newZeroCol);
    }

    public String toString() {
    	// Create a string rep of the state that is a square
        String rep = "";
        for (int i = 0; i < 9; i++) {
            rep += flatGrid[i];
            if ((i+1)%3 == 0) {
            	// end of row
                rep += "\n";
            } else {
                rep += " ";
            }
        }
        return rep;
    }

    public boolean equals(Object other) {
    	// If grids are equal, then states are equal
        return other instanceof EightPuzzleState && 
        		Arrays.equals(((EightPuzzleState) other).flatGrid, flatGrid);
    }

    public int hashCode() {
    	// Use hash code of the grid
    	/* This needs to be overriden so that the hashmap/hashset behaviour
    	 * is what we want when doing contains() operations.
    	 * The reason hash-based structures are used is because they can do
    	 * lookup operations efficiently in O(1) time.
    	 */
        return Arrays.hashCode(flatGrid);
    }
}
