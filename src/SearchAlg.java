/**
 * Created by Jordan on 9/08/2016.
 */
/**
 * Abstract class for a search algorithm.
 * 
 * Defines a start, goal, and and an abstract search method, which subclasses
 * override.
 * @author Jordan
 *
 */
public abstract class SearchAlg {

    private EightPuzzleState start;
    private EightPuzzleState goal;

    public SearchAlg(EightPuzzleState start, EightPuzzleState goal) {
        this.start = start;
        this.goal = goal;
    }

    public abstract void search();

    public EightPuzzleState getStart() {
        return start;
    }

    public EightPuzzleState getGoal() {
        return goal;
    }
}
