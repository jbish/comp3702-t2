import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Jordan on 8/08/2016.
 */
/**
 * Main class, the actual solver.
 * 
 * Run this with args: <puzzle_file> <search_alg>
 * 
 * where <search_alg> is either "BFS" or "DFS".
 * 
 * @author Jordan
 *
 */
public class EightPuzzleSolver {

    private static EightPuzzleState start;
    private static EightPuzzleState goal;

    public static void main(String[] args) {
    	// Get start and goal state from file
        try {
            readFile(args[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        SearchAlg alg = null;
        if (args[1].equals("BFS")) {
            alg = new BreadthFirstSearch(start, goal);
        } else if (args[1].equals("DFS")) {
            alg = new DepthFirstSearch(start, goal);
        }
        // Do the search
        alg.search();
        // If BFS was used, also print out shortest path
        if (alg instanceof BreadthFirstSearch) {
            ((BreadthFirstSearch) alg).printShortestPath();
        }
    }

    private static void readFile(String inputFileName) throws IOException {
        BufferedReader input = new BufferedReader(new FileReader(inputFileName));
        start = new EightPuzzleState(input.readLine());
        goal = new EightPuzzleState(input.readLine());
        input.close();
    }
}
